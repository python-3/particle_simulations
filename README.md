# Particle simulations

These are some particle simulation tests made from scratch, from the basic definition of the particles to the way they interact with each other and the way they are represented.
For representing the output of these simulations, the PIL module is being used.

The configurations for some of the simulations that have been tested are contained in the .yml file
You can see the output they produced in the following link:

https://vimeo.com/378892576

Just by tweaking the parameters contained in this configuration file, different results can be obtained.

### Recommended way of launching
Install the specified requirements and run _main.py_

### Output examples
![particle_outputs](http://www.jaimervq.com/wp-content/uploads/2020/01/particle_examples.jpg)